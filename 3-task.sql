select a.first_name,
       a.last_name,
       count(f.release_year),
       array_agg(f.release_year)
from actor a
         join film_actor fa on a.actor_id = fa.actor_id
         join film f on fa.film_id = f.film_id
group by a.actor_id
order by count(f.release_year)
