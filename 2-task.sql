select f.film_id,
       f.title,
       count(p.payment_id)
from film f
         join inventory i on f.film_id = i.film_id
         join rental r on i.inventory_id = r.inventory_id
         join payment p on r.rental_id = p.rental_id

group by f.film_id
order by count(p.payment_id) desc
limit 5